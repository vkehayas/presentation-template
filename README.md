# Instructions

To use this template repository for your Reveal.js/Rmarkdown presentation
copy its contents and edit the YAML header in file `source.Rmd`
to include the correct title (title shown in presentation),
pagetitle (title shown on the web browser's tab),
the link to the online application,
and dates.
Optionally also edit the `project-name.Rproj` file name.
After installing the `revealjs` R package 
and including the desired content for your presentation inside `source.Rmd`,
source the file `make.R` to compile the presentation.
This will create an `index.html` file
which you can push to your web host.
Images go inside the `img` directory.
